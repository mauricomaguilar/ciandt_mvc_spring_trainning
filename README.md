# Treinamento Spring MVC  
  
## Pré Requisitos

### 1. Downloads  
- **[Eclipse](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-09/R/eclipse-inst-win64.exe&mirror_id=1278/)**
- **[JDK 13](https://www.oracle.com/technetwork/java/javase/downloads/jdk13-downloads-5672538.html)**
- **[Maven](http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.6.2/source/apache-maven-3.6.2-src.zip)**

### 2. Configurações
- *Instale o eclipse*
- *Instale a JDK*
- - Abra variáveis de ambiente do Windows
- - Clique em **Novo...**
- - Em **Nome Variável** digite **JAVA_HOME**
- - Em **Valor Variável** preencha com o diretório que foi instalado a JDK - **C:\Program Files\Java\jdk-13.0.1**
- - Em **Variáveis Sistema** clique na variável **Path** e depois em **Edit...**
- - Clique em **New** e escreva **%JAVA_HOME%\bin**
- - Clique em **OK**
- *Descompacte o arquivo **apache-maven-3.6.1-src.zip** no diretório **C:\Users\[SEU_USUARIO]\Documents***
- - Abra variáveis de ambiente do Windows
- - Clique em **Novo...**
- - Em **Nome Variável** digite **M2_HOME**
- - Em **Valor Variável** preencha com o diretório **C:\Users\[SEU_USUARIO]\Documents\apache-maven-3.6.2**
- - Em **Variáveis Sistema** clique na variável **Path** e depois em **Edit...**
- - Clique em **New** e escreva **%M2_HOME%\bin**
- - Clique em **OK**

### 3. Iniciando ambiente
- **
